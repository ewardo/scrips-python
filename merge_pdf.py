from PyPDF2 import PdfFileReader, PdfFileWriter

file_name1 = 'pdf1'
file_name2 = 'pdf2'
file_name3 = 'merge' ###como quiere que se llame el resultado

pdf1File = open(file_name1+'.pdf', 'rb')
pdf2File = open(file_name2+'.pdf', 'rb')
pdf1Reader = PdfFileReader(pdf1File)
pdf2Reader = PdfFileReader(pdf2File)
pdfWriter = PdfFileWriter()

for pageNum in range(pdf1Reader.numPages):
    pageObj = pdf1Reader.getPage(pageNum)
    pdfWriter.addPage(pageObj)

for pageNum in range(pdf2Reader.numPages):
    pageObj = pdf2Reader.getPage(pageNum)
    pdfWriter.addPage(pageObj)

pdfOutputFile = open(file_name3+'.pdf', 'wb')
pdfWriter.write(pdfOutputFile)
pdfOutputFile.close()
pdf1File.close()
pdf2File.close()