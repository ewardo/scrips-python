from tabula import read_pdf

file_name = 'test'

df = read_pdf(file_name+".pdf", pages = 1)[0]
df.to_excel(file_name+'.xlsx')