#####Instalar estas librerias para cada caso que se necesite, 
#####solo se necesita ejecutar una vez, si no tiene PYTHON instale este antes.
#####puede descargar e instalar PYTHON desde este link https://www.python.org/downloads/

########LIBRERIAS#########
####pdf_to_word
pip install pdf2docx
pip install docx2pdf

####pdf_to_excel
pip install tabula-py

####txt_to_csv or csv_to_xlsx
pip install pandas
pip install openpyxl


#########Pasos a seguir para convertir archivos########
1. Crear un carpeta que contenga todos estos scrips
2. Agregar los documentos que se quieren transformar dentro de la misma carpeta
3. abrir el archivo a utilizar ejem "csv_to_xlsx", cambiamos el nombre de "test" en la variable 
   "file_name" y escribimos el nombre del archivo a convertir sin extencion
4. Abrir una terminal con la direccion de la carpeta creada 
5. Ejecutar el comando -> python "nombre del archivo".py, ejm: python csv_to_xlsx.py
