from pdf2docx import Converter, parse

file_name = 'Liquidaciones'

pdf_file = file_name+'.pdf'
docx_file = file_name+'.docx'
cv = Converter(pdf_file)
cv.convert(docx_file, start=0, end=None)
cv.close()
# parse(pdf_file, docx_file, start=0, end=None)