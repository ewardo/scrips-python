#!/usr/bin/python

from PyPDF2 import PdfFileReader, PdfFileWriter

pdf_document = "liquidaciones.pdf"
pdf = PdfFileReader(pdf_document)


for pageNum in range(pdf.numPages):
    pdfWriter = PdfFileWriter()
    pageObj = pdf.getPage(pageNum)
    pdfWriter.addPage(pageObj)

    outputFilename = "liquidaciones-{}.pdf".format(pageNum + 1)
    with open(outputFilename, "wb") as out:
        pdfWriter.write(out)
