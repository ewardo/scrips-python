import csv
import re


# with open('doc.txt', 'r') as in_file:    
#     stripped = (re.sub(r"\n+", ";", x) for x in in_file)
#     print([line for line in stripped])
#     lines = (line.split(",") for line in stripped if line)
#     with open('doc.csv', 'w') as out_file:
#         writer = csv.writer(out_file)
#         writer.writerows(lines)

nombre_archivo = "doc.txt"
with open(nombre_archivo, 'r') as in_file:    
    stripped = (re.sub(r"\t+", ";", x) for x in in_file)
    new = ""
    for line in stripped:
        if len(line) > 30:
            new = new + line.replace('\n','')
        else:
            new = new +';'+ line
    print(new)